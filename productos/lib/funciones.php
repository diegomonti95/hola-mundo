<?php

	function tabla(){
		echo"<table id='tabla'>
			<caption>Productos</caption>
			<tr>
				<td class='oscuro centrado'>Nombre</td>
				<td class='oscuro centrado'>Cantidad</td>
				<td class='oscuro centrado'>Precio(Gs)</td>
			</tr>
			<tr>
				<td>Coca Cola</td>
				<td class='centrado'>100</td>
				<td class='centrado'>4.500</td>
			</tr>
			<tr class='marcado'>
				<td>Pepsi</td>
				<td class='centrado'>30</td>
				<td class='centrado'>4.800</td>
			</tr>
			<tr>
				<td>Sprite</td>
				<td class='centrado'>20</td>
				<td class='centrado'>4.500</td>
			</tr>
			<tr class='marcado'>
				<td>Guarana</td>
				<td class='centrado'>200</td>
				<td class='centrado'>4.500</td>
			</tr>
			<tr>
				<td>SevenUP</td>
				<td class='centrado'>24</td>
				<td class='centrado'>4.800</td>
			</tr>
				<tr class='marcado'>
				<td>Mirinda Naranja</td>
				<td class='centrado'>56</td>
				<td class='centrado'>4.800</td>
			</tr>
			<tr>
				<td>Mirinda Guarana</td>
				<td class='centrado'>89</td>
				<td class='centrado'>4.800</td>
			</tr>
				<tr class='marcado'>
				<td>Fanta Naranja</td>
				<td class='centrado'>10</td>
				<td class='centrado'>4.500</td>
			</tr>
			<tr>
				<td>Fanta Piña</td>
				<td class='centrado'>2</td>
				<td class='centrado'>4.500</td>
			</tr>
		</table>";
	}
?>
